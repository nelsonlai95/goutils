package jwtpkg

import (
	"time"

	"github.com/golang-jwt/jwt"
)

func CreateJWT(payload map[string]interface{}, secret string, expired time.Duration) (string, error) {

	claims := jwt.MapClaims{}
	for k, v := range payload {
		claims[k] = v
	}

	// Renew expiry
	claims["exp"] = time.Now().Add(expired).Unix()

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString([]byte(secret))
}

func DecodeJWT(tokenString string, secret string) (map[string]interface{}, error) {

	claims := jwt.MapClaims{}
	_, err := jwt.ParseWithClaims(tokenString, claims, func(t *jwt.Token) (interface{}, error) {
		return []byte(secret), nil
	})
	if err != nil {
		return nil, err
	}

	payload := make(map[string]interface{})
	for k, v := range claims {
		payload[k] = v
	}
	return payload, nil
}
