package mongopkg

import (
	"go.mongodb.org/mongo-driver/bson"
)

type validator struct {
	required        []string
	properties      bson.M
	validationLevel string
}

func NewValidator() *validator {
	return &validator{}
}

func (v *validator) SetRequired(required string) *validator {
	v.required = append(v.required, required)
	return v
}

func (v *validator) SetProperties(field string, bsonType string) *validator {
	v.properties[field] = bsonType
	return v
}

func (v *validator) SetValidationLevel(level string) *validator {
	v.validationLevel = level
	return v
}

func (v *validator) Build() bson.M {
	return bson.M{
		"$jsonSchema": bson.D{
			{Key: "bsonType", Value: "object"},
			{Key: "required", Value: v.required},
			{Key: "properties", Value: v.properties},
		},
	}
}
