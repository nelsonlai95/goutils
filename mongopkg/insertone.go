package mongopkg

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type insertone struct {
	filter bson.M
	opts   *options.InsertOneOptions
}

func NewInsertOne() *insertone {
	insert := new(insertone)
	insert.opts = options.InsertOne()
	return insert
}

func (insert *insertone) SetFilter(filter bson.M) *insertone {
	insert.filter = filter
	return insert
}
