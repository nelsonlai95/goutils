package mongopkg

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type bulkwrite struct {
	opts       *options.BulkWriteOptions
	operations []mongo.WriteModel
}

func NewBulkWrite() *bulkwrite {
	bulk := &bulkwrite{}
	bulk.opts = options.BulkWrite()
	return bulk
}

func (b *bulkwrite) SetOrdered(ordered bool) *bulkwrite {
	b.opts.SetOrdered(ordered)
	return b
}

func (b *bulkwrite) SetUpdate(filter bson.M, update bson.M) *bulkwrite {
	b.operations = append(b.operations, mongo.NewUpdateOneModel().SetFilter(filter).SetUpdate(update).SetUpsert(true))
	return b
}

func (b *bulkwrite) SetDelete(filter bson.M) *bulkwrite {
	b.operations = append(b.operations, mongo.NewDeleteOneModel().SetFilter(filter))
	return b
}

func (b *bulkwrite) SetInsert(insert bson.M) *bulkwrite {
	b.operations = append(b.operations, mongo.NewInsertOneModel().SetDocument(insert))
	return b
}

func (b *bulkwrite) SetReplace(filter bson.M, replace bson.M) *bulkwrite {
	b.operations = append(b.operations, mongo.NewReplaceOneModel().SetFilter(filter).SetReplacement(replace).SetUpsert(true))
	return b
}

func (b *bulkwrite) SetUpdateMany(filter bson.M, update bson.M) *bulkwrite {
	b.operations = append(b.operations, mongo.NewUpdateManyModel().SetFilter(filter).SetUpdate(update).SetUpsert(true))
	return b
}

func (b *bulkwrite) SetDeleteMany(filter bson.M) *bulkwrite {
	b.operations = append(b.operations, mongo.NewDeleteManyModel().SetFilter(filter))
	return b
}
