package mongopkg

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type count struct {
	filter bson.M
	opts   *options.CountOptions
}

func NewCount() *count {
	c := &count{}
	c.opts = options.Count()
	return c
}

func (c *count) SetFilter(filter bson.M) *count {
	c.filter = filter
	return c
}

func (c *count) SetLimit(limit int64) *count {
	c.opts.SetLimit(limit)
	return c
}

func (c *count) SetSkip(skip int64) *count {
	c.opts.SetSkip(skip)
	return c
}
