package mongopkg

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type findone struct {
	filter bson.M
	opts   *options.FindOneOptions
}

func NewFindOne() *findone {
	f := new(findone)
	f.opts = options.FindOne()
	return f
}

// set filter
func (f *findone) SetFilter(filter bson.M) *findone {
	f.filter = filter
	return f
}

// set projection
func (f *findone) SetProjection(projection bson.M) *findone {
	f.opts.SetProjection(projection)
	return f
}
