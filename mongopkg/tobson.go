package mongopkg

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func ToBson(v interface{}) (primitive.M, error) {

	bsonByte, err := bson.Marshal(v)
	if err != nil {
		return nil, err
	}

	var bsonMap primitive.M
	err = bson.Unmarshal(bsonByte, &bsonMap)
	if err != nil {
		return nil, err
	}

	return bsonMap, nil
}
