package mongopkg

const (
	DOUBLE     = "double"
	STRING     = "string"
	OBJECT     = "object"
	ARRAY      = "array"
	BINARY     = "binData"
	UNDEFINED  = "undefined"
	OID        = "objectId"
	BOOLEAN    = "bool"
	DATE       = "date"
	NULL       = "null"
	REGEX      = "regex"
	DBPOINTER  = "dbPointer"
	JAVASCRIPT = "javascript"
	SYMBOL     = "symbol"
	JSSCOPE    = "codeWithScope"
	INT32      = "int"
	TIMESTAMP  = "timestamp"
	INT64      = "long"
	DECIMAL    = "decimal"
	MINKEY     = "minKey"
	MAXKEY     = "maxKey"
)

const (
	LV_STRICT   = "strict"
	LV_MODERATE = "moderate"
	LV_OFF      = "off"
)

const (
	OP_EQ         = "$eq"
	OP_NE         = "$ne"
	OP_GT         = "$gt"
	OP_GTE        = "$gte"
	OP_LT         = "$lt"
	OP_LTE        = "$lte"
	OP_IN         = "$in"
	OP_NIN        = "$nin"
	OP_AND        = "$and"
	OP_OR         = "$or"
	OP_NOR        = "$nor"
	OP_NOT        = "$not"
	OP_ALL        = "$all"
	OP_SIZE       = "$size"
	OP_ELEM_MATCH = "$elemMatch"
	OP_EXISTS     = "$exists"
	OP_TYPE       = "$type"
	OP_REGEX      = "$regex"
)
