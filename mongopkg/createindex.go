package mongopkg

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type createindexes struct {
	indexes []mongo.IndexModel
	opts    *options.CreateIndexesOptions
}

func NewCreateIndexes() *createindexes {
	create := &createindexes{}
	create.opts = options.CreateIndexes()
	return create
}

func (c *createindexes) SetIndex(field string, index int) *createindexes {
	indexModel := mongo.IndexModel{Keys: bson.M{field: index}}
	c.indexes = append(c.indexes, indexModel)
	return c
}
