package mongopkg

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type findmany struct {
	filter bson.M
	opts   *options.FindOptions
}

func NewFindMany() *findmany {
	f := new(findmany)
	f.opts = options.Find()
	return f
}

// set filter
func (f *findmany) SetFilter(filter bson.M) *findmany {
	f.filter = filter
	return f
}

// set sort
func (f *findmany) SetSort(sort bson.M) *findmany {
	f.opts.SetSort(sort)
	return f
}

// set limit
func (f *findmany) SetLimit(limit int64) *findmany {
	f.opts.SetLimit(limit)
	return f
}

// set skip
func (f *findmany) SetSkip(skip int64) *findmany {
	f.opts.SetSkip(skip)
	return f
}

// set projection
func (f *findmany) SetProjection(projection bson.M) *findmany {
	f.opts.SetProjection(projection)
	return f
}
