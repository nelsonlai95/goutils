package mongopkg

import (
	"context"
	"strconv"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoConnection struct {
	host     string
	port     int
	username string
	password string
	client   *mongo.Client
}

func NewMongoConnection(host string, port int, username string, password string) *MongoConnection {
	connect := &MongoConnection{
		host:     host,
		port:     port,
		username: username,
		password: password,
	}
	connect.connect()
	return connect
}

func (m *MongoConnection) Connect() {
	if m.client == nil {
		m.connect()
	}
	_, err := m.client.ListDatabaseNames(context.TODO(), nil)
	if err != nil {
		m.connect()
	}
}

func (m *MongoConnection) GetDatabaseNames() []string {
	m.Connect()
	names, err := m.client.ListDatabaseNames(context.TODO(), nil)
	if err != nil {
		panic(err)
	}
	return names
}

func (m *MongoConnection) GetDatabase(name string) *Database {
	m.Connect()
	return newDatabase(m, m.client, name)
}

/* Private functions */

func (m *MongoConnection) connect() {
	option := options.Client()
	option.ApplyURI("mongodb://" + m.username + ":" + m.password + "@" + m.host + ":" + strconv.Itoa(m.port) + "/")

	var err error
	m.client, err = mongo.Connect(context.TODO(), option)
	if err != nil {
		panic(err)
	}
}
