package mongopkg

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type Collection struct {
	conn   *MongoConnection
	client *mongo.Client
	db     *mongo.Database
	coll   *mongo.Collection
}

func newCollection(conn *MongoConnection, client *mongo.Client, db *mongo.Database, coll string) *Collection {
	return &Collection{
		conn:   conn,
		client: client,
		db:     db,
		coll:   db.Collection(coll),
	}
}

func (coll *Collection) Connect() {
	coll.conn.Connect()
}

func (coll *Collection) CreateIndex(opt *createindexes) error {
	_, err := coll.coll.Indexes().CreateMany(context.TODO(), opt.indexes, opt.opts)
	return err
}

func (coll *Collection) CreateValidator(opt *validator) error {
	cmd := bson.D{
		primitive.E{Key: "collMod", Value: coll.coll.Name()},
		primitive.E{Key: "validator", Value: opt.Build()},
		primitive.E{Key: "validationLevel", Value: opt.validationLevel},
		primitive.E{Key: "validationAction", Value: "error"},
	}
	err := coll.db.RunCommand(context.TODO(), cmd).Err()
	return err
}

func (coll *Collection) FindOne(opt *findone, model interface{}) error {
	return coll.coll.FindOne(context.TODO(), opt.filter, opt.opts).Decode(model)
}

func (coll *Collection) FindMany(opt *findmany) (*cursor, error) {
	cur, err := coll.coll.Find(context.TODO(), opt.filter, opt.opts)
	if err != nil {
		return nil, err
	}
	return NewCursor(cur), nil
}

func (coll *Collection) BulkWrite(opt *bulkwrite) error {
	_, err := coll.coll.BulkWrite(context.TODO(), opt.operations, opt.opts)
	return err
}

func (coll *Collection) InsertOne(opt *insertone, model interface{}) error {
	_, err := coll.coll.InsertOne(context.TODO(), model, opt.opts)
	return err
}

func (coll *Collection) UpdateOne(opt *updateone, model interface{}) error {
	_, err := coll.coll.UpdateOne(context.TODO(), opt.filter, model, opt.opts)
	return err
}

func (coll *Collection) InsertMany(opt *insertmany) error {
	_, err := coll.coll.InsertMany(context.TODO(), opt.filter, opt.opts)
	return err
}

func (coll *Collection) UpdateMany(opt *updatemany, model interface{}) error {
	_, err := coll.coll.UpdateMany(context.TODO(), opt.filter, model, opt.opts)
	return err
}

func (coll *Collection) DeleteOne(opt *deleteone) error {
	_, err := coll.coll.DeleteOne(context.TODO(), opt.filter, opt.opts)
	return err
}

func (coll *Collection) DeleteMany(opt *deletemany) error {
	_, err := coll.coll.DeleteMany(context.TODO(), opt.filter, opt.opts)
	return err
}

func (coll *Collection) Aggreate(opt *aggreate) (*cursor, error) {
	cur, err := coll.coll.Aggregate(context.TODO(), opt.piplines, opt.opts)
	if err != nil {
		return nil, err
	}
	return NewCursor(cur), nil
}

func (coll *Collection) Drop() error {
	return coll.coll.Drop(context.TODO())
}

func (coll *Collection) Count(opt *count) (int64, error) {
	count, err := coll.coll.CountDocuments(context.TODO(), opt.filter, opt.opts)
	return count, err
}

func (coll *Collection) Distinct(opt *distinct) error {
	_, err := coll.coll.Distinct(context.TODO(), opt.field, opt.filter, opt.opts)
	return err
}

func (coll *Collection) ListIndexes() (*cursor, error) {
	cur, err := coll.coll.Indexes().List(context.TODO(), nil)
	if err != nil {
		return nil, err
	}
	return NewCursor(cur), nil
}
