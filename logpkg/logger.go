package logpkg

import (
	"os"
	"runtime/debug"
	"time"
)

/*
	Log Format:
		[Type] [Time] [Caller] [Message]
*/

type message struct {
	msgType string
	time    string
	caller  string
	msg     string
}

type logger struct {
	caller     string
	savefolder string
}

func NewLogger(caller string) *logger {

	var savefolder string
	if LOG_SAVE_PATH == "" {
		savefolder = "./log"
	} else {
		savefolder = LOG_SAVE_PATH
	}
	os.MkdirAll(savefolder, os.ModePerm)

	return &logger{caller: caller, savefolder: savefolder}
}

func (l *logger) Info(msg string) {
	now := time.Now().Format("2006-01-02 15:04:05")
	l.saveLog(message{msgType: "INFO ", time: now, caller: l.caller, msg: msg})
	println(l.colorMsgType("INFO ") + "\t" + Cyan(now) + "\t" + Purple(l.caller) + "\t|" + msg)
}

func (l *logger) Warn(msg string) {
	now := time.Now().Format("2006-01-02 15:04:05")
	l.saveLog(message{msgType: "WARN ", time: now, caller: l.caller, msg: msg})
	println(l.colorMsgType("WARN ") + "\t" + Cyan(now) + "\t" + Purple(l.caller) + "\t|" + msg)
}

func (l *logger) Error(err error) {
	now := time.Now().Format("2006-01-02 15:04:05")
	l.saveLog(message{msgType: "ERROR", time: now, caller: l.caller, msg: err.Error() + "\n" + string(debug.Stack())})
	println(l.colorMsgType("ERROR") + "\t" + Cyan(now) + "\t" + Purple(l.caller) + "\t|" + err.Error() + "\n" + string(debug.Stack()))
}

func (l *logger) Debug(msg string) {
	now := time.Now().Format("2006-01-02 15:04:05")
	println(l.colorMsgType("DEBUG") + "\t" + Cyan(now) + "\t" + Purple(l.caller) + "\t|" + msg)
}

/* Private functions */

func (l *logger) saveLog(msg message) {
	filename := l.savefolder + "/log_" + time.Now().Format("2006-01-02_15") + ".log"
	f, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY|os.O_APPEND, os.ModePerm)
	if err != nil {
		panic(err)
	}
	_, err = f.WriteString(msg.msgType + "\t" + msg.time + "\t" + msg.caller + "\t|" + msg.msg + "\n")
	if err != nil {
		panic(err)
	}
	f.Close()
}

func (l *logger) colorMsgType(msgType string) string {
	switch msgType {
	case "INFO ":
		return Green(msgType)
	case "WARN ":
		return Yellow(msgType)
	case "ERROR":
		return Red(msgType)
	case "DEBUG":
		return Blue(msgType)
	}
	return msgType
}
