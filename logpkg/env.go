package logpkg

import (
	"gitlab.com/nelsonlai95/gopkg/configpkg"
)

var LOG_SAVE_PATH = configpkg.GetEnvString("LOG_SAVE_PATH", "./logs")
