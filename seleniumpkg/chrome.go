package seleniumpkg

import "github.com/tebeka/selenium"

type Chrome struct {
	service *selenium.Service
	driver  selenium.WebDriver
}

func NewChrome(driverPath string) *Chrome {
	service, err := selenium.NewChromeDriverService(driverPath, 9222)
	if err != nil {
		panic(err)
	}

	caps := selenium.Capabilities{
		"browserName": "chrome",
	}

	wd, err := selenium.NewRemote(caps, "http://localhost:9222/wd/hub")
	if err != nil {
		panic(err)
	}

	return &Chrome{service, wd}
}

func (c *Chrome) Close() {
	c.driver.Quit()
	c.service.Stop()
}

func (c *Chrome) Get(url string) {
	c.driver.Get(url)
}

func (c *Chrome) FindElementByXPath(xpath string) (selenium.WebElement, error) {
	return c.driver.FindElement(selenium.ByXPATH, xpath)
}
