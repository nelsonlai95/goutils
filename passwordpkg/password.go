package passwordpkg

import "golang.org/x/crypto/bcrypt"

func Encode(password string) string {
	return hash(password)
}

func Match(password string, hash string) bool {
	return match(password, hash)
}

/* Private functions */

func hash(password string) string {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		panic(err)
	}
	return string(hash)
}

func match(password string, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
